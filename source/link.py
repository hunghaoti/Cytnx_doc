## Add links to following, remember %s is neeeded for url!
extlinks = {'splo':("https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.LinearOperator.html%s",''),\
            'torch-tn':("https://pytorch.org/docs/stable/tensors.html%s",''),\
            'numpy-arr':("https://numpy.org/doc/1.18/reference/generated/numpy.array.html%s",''),\
            'numpy-slice':("https://numpy.org/doc/stable/reference/arrays.indexing.html%s",''),\
            'numpy-vstack':("https://numpy.org/doc/stable/reference/generated/numpy.vstack.html%s",''),\
            'cuda-mem':("https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html%s",''),\
            'lanczos-er':("https://www.sciencedirect.com/science/article/pii/S0010465597001367%s",''),\
            'anaconda':("https://repo.anaconda.com/archive/%s",''),\
            'miniconda':("https://repo.anaconda.com/miniconda/%s",''),\
            'virtualenv':("https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#%s",''),\
            'mkl-mac':("https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library/choose-download/macos.html%s",''),\
            'wsl':("https://docs.microsoft.com/en-us/windows/wsl/install-win10%s",'')\
            } 
