Bond
=======
A **Bond** is an object that represents the legs or indices of a tensor. It carries information such as the direction, dimension and quantum numbers (if symmetries given). 

There are in general two types of Bonds: **directional** and **undirectional** depending on whether the bond has a direction (pointing inward or outward with respect to the tensor) or not. The inward Bond is also defined as **Ket**/**In** type, while the outward Bond is defined as **Bra**/**Out** type as in the *Braket* notation in the quantum mechanics: 

.. image:: image/bond.png
    :width: 400
    :align: center

The API for constructing a simple Bond (with or without direction) is:

.. py:function:: Bond(dim, bd_type)
     
    :param int dim: The dimension of the bond.
    :param bondType bd_type: The type (direction) of the bond, can be BD_REG--undirectional, BD_KET--inward (same as BD_IN), BD.BRA--outward (same as BD_OUT)





Symmetry object
**********************

Symmetries play an important role in physical simulations. Tensors and bonds can be defined in a way that preserves the symmetries. This helps to reduce the numerical costs, can increase precision and it allows to do calculations restricted to specific parity sectors.

In Cytnx, the type of symmetry is defined by a Symmetry object. It contains the name, type, combine rule and the reverse rule of that symmetry. The combine rule contains the information how two quantum numbers are combined to a new quantum number. Let us create Symmetry objects for a U1 and a Z_2 symmetry and print their info:

* In Python:

.. code-block:: python
    :linenos:


    sym_u1 = cytnx.Symmetry.U1()
    sym_z2 = cytnx.Symmetry.Zn(2)
    print(sym_u1)
    print(sym_z2)
    
* In C++:

.. code-block:: c++
    :linenos:

    Symmetry sym_u1 = cytnx::Symmetry::U1();
    Symmetry sym_z2 = cytnx::Symmetry::Zn(2);

    cout << sym_u1 << endl;
    cout << sym_z2 << endl;

Output >>

.. code-block:: text

    --------------------
    [Symmetry]
    type : Abelian, U1
    combine rule : Q1 + Q2
    reverse rule : Q*(-1) 
    --------------------

    --------------------
    [Symmetry]
    type : Abelian, Z(2)
    combine rule : (Q1 + Q2)%2
    reverse rule : Q*(-1) 
    --------------------


Create Bond with Qnums
*****************************

In order to implement symmetries on the level of tensors, we assign a quantum number to each value of an index. The quantum numbers can have a degeneracy, such that several values of an index correspond to the same quantum number.

To construct a Bond with symmetries and associate quantum numbers, the following API can be used:

.. py:function:: Bond(bd_type, qnums_list , degeneracies, sym_list)
     
    :param bondType bd_type: type (direction) of the bond, this can ONLY be BD_KET--inward (BD_IN) or BD_BRA--outward (BD_OUT) when quantum numbers are used 
    :param list qnums_list: quantum number list 
    :param list degeneracies: degeneracies (dimensions) of the qnums 
    :param list sym_list: list of Symmetry objects that define the symmetry of each qnum


    
The two arguments *qnums_list* and *degeneracies* can be combined into a single argument, for example:

* In Python:

.. code-block:: python 
    :linenos:
    
    # This creates an KET (IN) Bond with quantum number 0,-4,-2,3 with degs 3,4,3,2 respectively.
    bd_sym_u1_a = cytnx.Bond(cytnx.BD_KET,[cytnx.Qs(0)>>3,cytnx.Qs(-4)>>4,cytnx.Qs(-2)>>3,cytnx.Qs(3)>>2],[cytnx.Symmetry.U1()])

    # equivalent:
    bd_sym_u1_a = cytnx.Bond(cytnx.BD_IN,[cytnx.Qs(0),cytnx.Qs(-4),cytnx.Qs(-2),cytnx.Qs(3)],[3,4,3,2],[cytnx.Symmetry.U1()])

    print(bd_sym_u1_a)

* In C++:

.. code-block:: c++
    :linenos:
    
    Bond bd_sym_u1_a = cytnx::Bond(cytnx::BD_KET,{cytnx::Qs(0)>>3,cytnx::Qs(-4)>>4,cytnx::Qs(-2)>>3,cytnx::Qs(3)>>2},{cytnx::Symmetry::U1()});
    
    Bond bd_sym_u1_a = cytnx::Bond(cytnx::BD_IN,{cytnx::Qs(0),cytnx::Qs(-4),cytnx::Qs(-2),cytnx::Qs(3)},{0,4,3,2},{cytnx::Symmetry::U1()});

    print(bd_sym_u1_a);

Output >>

.. code-block:: text

    Dim = 12 |type: KET>     
     U1::   +0  -4  -2  +3
    Deg>>    3   4   3   2


In some cases, we might want to include multiple symmetries in the system. For example: U1 x Z2, which can be achieve by adding additional quantum numbers inside *Qs()*


.. code-block:: python 
    :linenos:

    # This creates a KET (IN) Bond with U1xZ2 symmetry and quantum numbers (0,0),(-4,1),(-2,0),(3,1) with degs 3,4,3,2 respectively.
    bd_sym_u1z2_a = cytnx.Bond(cytnx.BD_KET,\
                               [cytnx.Qs(0 ,0)>>3,\
                                cytnx.Qs(-4,1)>>4,\
                                cytnx.Qs(-2,0)>>3,\
                                cytnx.Qs(3 ,1)>>2],\
                               [cytnx.Symmetry.U1(),cytnx.Symmetry.Zn(2)])

    print(bd_sym_u1z2_a)


* In C++:

.. code-block:: c++
    :linenos:

    auto bd_sym_u1z2_a = cytnx::Bond(cytnx::BD_KET,
                                     {cytnx::Qs(0 ,0)>>3,
                                      cytnx::Qs(-4,1)>>4,
                                      cytnx::Qs(-2,0)>>3,
                                      cytnx::Qs(3 ,1)>>2},
                                     {cytnx::Symmetry::U1(),cytnx::Symmetry::Zn(2)});

    print(bd_sym_u1z2_a);
    

.. code-block:: text

    Dim = 12 |type: KET>     
     U1::   +0  -4  -2  +3
     Z2::   +0  +1  +0  +1
    Deg>>    3   4   3   2



Combine Bonds
*****************

In typical algorithms, two bonds often get combined to one bond. This can be done with Bonds involving Symmetries as well. The quantum numbers are merged according to the combine rules.

As an example, let us create another U1 bond **bd_sym_u1_c** and combine it with **bd_sym_u1_a**:

* In Python:

.. code-block:: python
    :linenos:

    bd_sym_u1_c = cytnx.Bond(cytnx.BD_KET,[cytnx.Qs(-1)>>2,cytnx.Qs(1)>>3,cytnx.Qs(2)>>4,cytnx.Qs(-2)>>5,cytnx.Qs(0)>>6])
    print(bd_sym_u1_c)

    bd_sym_all = bd_sym_u1_a.combineBond(bd_sym_u1_c)
    print(bd_sym_all)


Output >>

.. code-block:: text

    Dim = 20 |type: KET>     
    U1::   -1  +1  +2  -2  +0
    Deg>>    2   3   4   5   6


    Dim = 240 |type: KET>     
     U1::   -6  -5  -4  -3  -2  -1  +0  +1  +2  +3  +4  +5
    Deg>>   20   8  39  18  49  15  30  19  16  12   6   8


Here, we can observe the quantum numbers of **bd_sym_u1_a** combine with **bd_sym_u1_c** and generate 12 quantum numbers, respecting the combine rule (addition) of the U1 symmetry.


.. note::

    The Bonds need to be in the same direction to be combined. As a physical interpretation, one cannot combine a ket state with a bra state! 

.. warning::

    When no symmetry argument is given in the creation of a bond with quantum numbers, U1 is assumed by default as the symmetry group. 



.. tip::
    
    Using **combineBond_()** (with underscore) will modify the instance directly (as the general convention with underscore indicates inplace). 

By default, combineBond will group any quantum numbers of the same type together. Generally, the quantum number of merging two Bonds should be similar to Kron, and sometimes user might want to keep the order instead. In such scenarios, one can set the additional argument **is_grp = False**:


* In Python:

.. code-block:: Python
    :linenos:

    bd_sym_all = bd_sym_u1_a.combineBond(bd_sym_u1_c,is_grp=False)
    print(bd_sym_all)


.. code-block:: text
    

    Dim = 240 |type: KET>     
     U1::   -1  +1  +2  -2  +0  -5  -3  -2  -6  -4  -3  -1  +0  -4  -2  +2  +4  +5  +1  +3
    Deg>>    6   9  12  15  18   8  12  16  20  24   6   9  12  15  18   4   6   8  10  12

.. warning::

    This is not efficient since duplicate quantum number can occur. A warning will be thrown when is_grp=False is used.



.. toctree::

