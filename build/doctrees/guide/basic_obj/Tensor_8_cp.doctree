����      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�When will data be copied?�h]�h	�Text����When will data be copied?�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�H/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/basic_obj/Tensor_8_cp.rst�hKubh	�	paragraph���)��}�(hXK  There are two elementary operations of **cytnx.Tensor** that are very important: **permute** and **reshape**. These two operations are strategically designed to avoid redundant copy operations as much as possible. **cytnx.Tensor** follows the same principles as **numpy.array** and **torch.Tensor** concerning these two operations.�h]�(h�'There are two elementary operations of �����}�(hh/hhhNhNubh	�strong���)��}�(h�**cytnx.Tensor**�h]�h�cytnx.Tensor�����}�(hh9hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh� that are very important: �����}�(hh/hhhNhNubh8)��}�(h�**permute**�h]�h�permute�����}�(hhKhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh� and �����}�(hh/hhhNhNubh8)��}�(h�**reshape**�h]�h�reshape�����}�(hh]hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh�j. These two operations are strategically designed to avoid redundant copy operations as much as possible. �����}�(hh/hhhNhNubh8)��}�(h�**cytnx.Tensor**�h]�h�cytnx.Tensor�����}�(hhohhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh�  follows the same principles as �����}�(hh/hhhNhNubh8)��}�(h�**numpy.array**�h]�h�numpy.array�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh� and �����}�hh/sbh8)��}�(h�**torch.Tensor**�h]�h�torch.Tensor�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh�! concerning these two operations.�����}�(hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h�<The following figure shows the structure of a Tensor object:�h]�h�<The following figure shows the structure of a Tensor object:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�image���)��}�(h�A.. image:: image/Tnbasic.png
    :width: 500
    :align: center

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��500��align��center��uri��!guide/basic_obj/image/Tnbasic.png��
candidates�}��*�h�suh+h�hhhhhh,hNubh.)��}�(h��Two important concepts need to be distinguished: the Tensor **object** itself, and the things that are stored inside a Tensor object. Each Tensor object contains two ingredients:�h]�(h�<Two important concepts need to be distinguished: the Tensor �����}�(hh�hhhNhNubh8)��}�(h�
**object**�h]�h�object�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�l itself, and the things that are stored inside a Tensor object. Each Tensor object contains two ingredients:�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�block_quote���)��}�(h��1. the **meta** contains all the data that describe the attributes of the Tensor, like the shape and the number of elements
2. a **Storage** that contains the data (the actual tensor elements) which are stored in memory.

�h]�h	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(h�xthe **meta** contains all the data that describe the attributes of the Tensor, like the shape and the number of elements�h]�h.)��}�(hh�h]�(h�the �����}�(hh�hhhNhNubh8)��}�(h�**meta**�h]�h�meta�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh�l contains all the data that describe the attributes of the Tensor, like the shape and the number of elements�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�)��}�(h�_a **Storage** that contains the data (the actual tensor elements) which are stored in memory.

�h]�h.)��}�(h�]a **Storage** that contains the data (the actual tensor elements) which are stored in memory.�h]�(h�a �����}�(hj(  hhhNhNubh8)��}�(h�**Storage**�h]�h�Storage�����}�(hj0  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj(  ubh�P that contains the data (the actual tensor elements) which are stored in memory.�����}�(hj(  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj$  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+h�hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Reference & Copy of object�h]�h�Reference & Copy of object�����}�(hjb  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj_  hhhh,hKubh.)��}�(h�}One of the most important features in Python is the *referencing* of objects. All the Cytnx objects follow the same behavior:�h]�(h�4One of the most important features in Python is the �����}�(hjp  hhhNhNubh	�emphasis���)��}�(h�*referencing*�h]�h�referencing�����}�(hjz  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hjp  ubh�< of objects. All the Cytnx objects follow the same behavior:�����}�(hjp  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj_  hhubh	�bullet_list���)��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet�h�uh+j�  hh,hKhj_  hhubh	�literal_block���)��}�(h�-A = cytnx.zeros([3,4,5])
B = A

print(B is A)�h]�h�-A = cytnx.zeros([3,4,5])
B = A

print(B is A)�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��linenos���force���language��python��highlight_args�}�uh+j�  hh,hKhj_  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK"hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK"hj_  hhubj�  )��}�(h�Eauto A = cytnx::zeros({3,4,5});
auto B = A;

cout << is(B,A) << endl;�h]�h�Eauto A = cytnx::zeros({3,4,5});
auto B = A;

cout << is(B,A) << endl;�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hK$hj_  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK,hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK,hj_  hhubj�  )��}�(h�True�h]�h�True�����}�hj   sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hK.hj_  hhubh.)��}�(h��Here, **B** is a reference of **A**, so essentially **B** and **A** are the same object. We can use **is** to check if two objects are the same. Since they are the same object, all the change made to **B** will affect **A** as well.�h]�(h�Here, �����}�(hj0  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj8  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh� is a reference of �����}�(hj0  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hjJ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh�, so essentially �����}�(hj0  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj\  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh� and �����}�(hj0  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hjn  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh�! are the same object. We can use �����}�(hj0  hhhNhNubh8)��}�(h�**is**�h]�h�is�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh�^ to check if two objects are the same. Since they are the same object, all the change made to �����}�(hj0  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh� will affect �����}�(hj0  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj0  ubh�	 as well.�����}�(hj0  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hj_  hhubh.)��}�(h��To really create a copy of **A**, we can use the **clone()** method. **clone()** creates a new object with copied meta data and a newly allocated **Storage** with the same content as the storage of **A**:�h]�(h�To really create a copy of �����}�(hj�  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�, we can use the �����}�(hj�  hhhNhNubh8)��}�(h�**clone()**�h]�h�clone()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�	 method. �����}�(hj�  hhhNhNubh8)��}�(h�**clone()**�h]�h�clone()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�B creates a new object with copied meta data and a newly allocated �����}�(hj�  hhhNhNubh8)��}�(h�**Storage**�h]�h�Storage�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�) with the same content as the storage of �����}�(hj�  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK4hj_  hhubj�  )��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj+  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK6hj'  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj$  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK6hj_  hhubj�  )��}�(h�5A = cytnx.zeros([3,4,5])
B = A.clone()

print(B is A)�h]�h�5A = cytnx.zeros([3,4,5])
B = A.clone()

print(B is A)�����}�hjE  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hK8hj_  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj\  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hjX  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjU  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK@hj_  hhubj�  )��}�(h�Mauto A = cytnx::zeros({3,4,5});
auto B = A.clone();

cout << is(B,A) << endl;�h]�h�Mauto A = cytnx::zeros({3,4,5});
auto B = A.clone();

cout << is(B,A) << endl;�����}�hjv  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hKBhj_  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKJhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hKJhj_  hhubj�  )��}�(h�False�h]�h�False�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hKLhj_  hhubeh}�(h!]��reference-copy-of-object�ah#]�h%]��reference & copy of object�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Permute�h]�h�Permute�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKRubh.)��}�(h�[Now let us take a look at what happens if we perform a **permute()** operation on a Tensor:�h]�(h�7Now let us take a look at what happens if we perform a �����}�(hj�  hhhNhNubh8)��}�(h�**permute()**�h]�h�	permute()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� operation on a Tensor:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKThj�  hhubj�  )��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKVhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hKVhj�  hhubj�  )��}�(h�OA = cytnx.zeros([2,3,4])
B = A.permute(0,2,1)

print(A)
print(B)

print(B is A)�h]�h�OA = cytnx.zeros([2,3,4])
B = A.permute(0,2,1)

print(A)
print(B)

print(B is A)�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hKXhj�  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj(  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKchj$  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj!  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hKchj�  hhubj�  )��}�(h�zauto A = cytnx::zeros({2,3,4})
auto B = A.permute(0,2,1);

cout << A << endl;
cout << B << endl;

cout << is(B,A) << endl;�h]�h�zauto A = cytnx::zeros({2,3,4})
auto B = A.permute(0,2,1);

cout << A << endl;
cout << B << endl;

cout << is(B,A) << endl;�����}�hjB  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hKehj�  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hjY  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKphjU  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjR  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hKphj�  hhubj�  )��}�(hX)  Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]]


Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]


False�h]�hX)  Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]]


Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]


False�����}�hjs  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hKrhj�  hhubh.)��}�(h��We see that **A** and **B** are now two different objects (as it should be, they have different shapes!). Now let's see what happens if we change an element in **A**:�h]�(h�We see that �����}�(hj�  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� and �����}�(hj�  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�� are now two different objects (as it should be, they have different shapes!). Now let’s see what happens if we change an element in �����}�(hj�  hhhNhNubh8)��}�(h�**A**�h]�h�A�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�!A[0,0,0] = 300

print(A)
print(B)�h]�h�!A[0,0,0] = 300

print(A)
print(B)�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�6A(0,0,0) = 300;

cout << A << endl;
cout << B << endl;�h]�h�6A(0,0,0) = 300;

cout << A << endl;
cout << B << endl;�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hj0  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj,  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj)  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(hX   Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[3.00000e+02 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]]

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[3.00000e+02 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]�h]�hX   Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,3,4)
[[[3.00000e+02 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]]]

Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[3.00000e+02 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]�����}�hjJ  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hK�hj�  hhubh.)��}�(hX  Notice that the element in **B** is also changed! So what actually happened? When we called **permute()**, a new object was created, which has different *meta*, but the two Tensors actually share the *same* data storage! There is NO copy of the tensor elements in memory performed:�h]�(h�Notice that the element in �����}�(hjZ  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hjb  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hjZ  ubh�< is also changed! So what actually happened? When we called �����}�(hjZ  hhhNhNubh8)��}�(h�**permute()**�h]�h�	permute()�����}�(hjt  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hjZ  ubh�0, a new object was created, which has different �����}�(hjZ  hhhNhNubjy  )��}�(h�*meta*�h]�h�meta�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hjZ  ubh�), but the two Tensors actually share the �����}�(hjZ  hhhNhNubjy  )��}�(h�*same*�h]�h�same�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hjZ  ubh�K data storage! There is NO copy of the tensor elements in memory performed:�����}�(hjZ  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(h�?.. image:: image/Tnsdat.png
    :width: 500
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��500��align��center��uri�� guide/basic_obj/image/Tnsdat.png�h�}�h�j�  suh+h�hj�  hhhh,hNubh.)��}�(h�XWe can use **Tensor.same_data()** to check if two objects share the same memory storage:�h]�(h�We can use �����}�(hj�  hhhNhNubh8)��}�(h�**Tensor.same_data()**�h]�h�Tensor.same_data()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�7 to check if two objects share the same memory storage:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�print(B.same_data(A))�h]�h�print(B.same_data(A))�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�cout << B.same_data(A) << endl;�h]�h�cout << B.same_data(A) << endl;�����}�hj3  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hjJ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjF  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjC  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�True�h]�h�True�����}�hjd  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hK�hj�  hhubh.)��}�(h�>As you can see, **permute()** never copies the memory storage.�h]�(h�As you can see, �����}�(hjt  hhhNhNubh8)��}�(h�**permute()**�h]�h�	permute()�����}�(hj|  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hjt  ubh�! never copies the memory storage.�����}�(hjt  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubeh}�(h!]��permute�ah#]�h%]��permute�ah']�h)]�uh+h
hhhhhh,hKRubh)��}�(hhh]�(h)��}�(h�
Contiguous�h]�h�
Contiguous�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(hX�  Next, let's have a look at the **contiguous** property. In the above example, we see that **permute()** created a new Tensor object with different *meta* but sharing the same memory storage. The memory layout of the **B** Tensor no longer corresponds to the tensors shape after the permutation. A Tensor in with this status is called **non-contiguous**. We can use **is_contiguous()** to check if a Tensor is with this status.�h]�(h�!Next, let’s have a look at the �����}�(hj�  hhhNhNubh8)��}�(h�**contiguous**�h]�h�
contiguous�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�- property. In the above example, we see that �����}�(hj�  hhhNhNubh8)��}�(h�**permute()**�h]�h�	permute()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�, created a new Tensor object with different �����}�(hj�  hhhNhNubjy  )��}�(h�*meta*�h]�h�meta�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj�  ubh�? but sharing the same memory storage. The memory layout of the �����}�(hj�  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�q Tensor no longer corresponds to the tensors shape after the permutation. A Tensor in with this status is called �����}�(hj�  hhhNhNubh8)��}�(h�**non-contiguous**�h]�h�non-contiguous�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�. We can use �����}�(hj�  hhhNhNubh8)��}�(h�**is_contiguous()**�h]�h�is_contiguous()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�* to check if a Tensor is with this status.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj.  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj*  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj'  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�`A = cytnx.zeros([2,3,4])
B = A.permute(0,2,1)

print(A.is_contiguous())
print(B.is_contiguous())�h]�h�`A = cytnx.zeros([2,3,4])
B = A.permute(0,2,1)

print(A.is_contiguous())
print(B.is_contiguous())�����}�hjH  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj_  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj[  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjX  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h��auto A = cytnx::zeros({2,3,4})
auto B = A.permute(0,2,1);

cout << A.is_contiguous() << endl;
cout << B.is_contiguous() << endl;�h]�h��auto A = cytnx::zeros({2,3,4})
auto B = A.permute(0,2,1);

cout << A.is_contiguous() << endl;
cout << B.is_contiguous() << endl;�����}�hjy  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hK�hj�  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hK�hj�  hhubj�  )��}�(h�
True
False�h]�h�
True
False�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hM hj�  hhubh.)��}�(h��We can make a contiguous Tensor **C** that has the same shape as **B** by calling **contiguous()**. Creating such a contiguous Tensor requires moving the elements in memory to their right position, matching the shape of Tensor.�h]�(h� We can make a contiguous Tensor �����}�(hj�  hhhNhNubh8)��}�(h�**C**�h]�h�C�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� that has the same shape as �����}�(hj�  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� by calling �����}�(hj�  hhhNhNubh8)��}�(h�**contiguous()**�h]�h�contiguous()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh��. Creating such a contiguous Tensor requires moving the elements in memory to their right position, matching the shape of Tensor.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMhj�  hhubh�)��}�(h�A.. image:: image/Tncontg.png
    :width: 650
    :align: center

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��650��align��center��uri��!guide/basic_obj/image/Tncontg.png�h�}�h�j  suh+h�hj�  hhhh,hNubj�  )��}�(h�LC = B.contiguous()

print(C)
print(C.is_contiguous())

print(C.same_data(B))�h]�h�LC = B.contiguous()

print(C)
print(C.is_contiguous())

print(C.same_data(B))�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �python�j�  }�uh+j�  hh,hMhj�  hhubj�  )��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj&  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMhj"  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hMhj�  hhubj�  )��}�(h�oauto C = B.contiguous()

cout << C << endl;
cout << C.is_contiguous() << endl;

cout << C.same_data(B) << endl;�h]�h�oauto C = B.contiguous()

cout << C << endl;
cout << C.is_contiguous() << endl;

cout << C.same_data(B) << endl;�����}�hj@  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �j�  �c++�j�  }�uh+j�  hh,hMhj�  hhubj�  )��}�(hhh]�h�)��}�(h�output:
�h]�h.)��}�(h�output:�h]�h�output:�����}�(hjW  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hM%hjS  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjP  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  h�uh+j�  hh,hM%hj�  hhubj�  )��}�(hX�  Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]

True
False�h]�hX�  Total elem: 24
type  : Double (Float64)
cytnx device: CPU
Shape : (2,4,3)
[[[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]
 [[0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]
  [0.00000e+00 0.00000e+00 0.00000e+00 ]]]

True
False�����}�hjq  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hM'hj�  hhubh	�hint���)��}�(h��We can also make **B** itself contiguous by calling **B.contiguous_()** (with underscore). Notice that this will create a new internal storage for **B**, so after calling **B.contiguous_()**, **B.same_data(A)** will be false!�h]�h.)��}�(hj�  h]�(h�We can also make �����}�(hj�  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� itself contiguous by calling �����}�(hj�  hhhNhNubh8)��}�(h�**B.contiguous_()**�h]�h�B.contiguous_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�L (with underscore). Notice that this will create a new internal storage for �����}�(hj�  hhhNhNubh8)��}�(h�**B**�h]�h�B�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�, so after calling �����}�(hj�  hhhNhNubh8)��}�(h�**B.contiguous_()**�h]�h�B.contiguous_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�, �����}�(hj�  hhhNhNubh8)��}�(h�**B.same_data(A)**�h]�h�B.same_data(A)�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh� will be false!�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hM?hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  hhhh,hNubh	�note���)��}�(h�~calling **contiguous()** on a Tensor that already has contiguous status will return itself, and no new object will be created!�h]�h.)��}�(hj�  h]�(h�calling �����}�(hj�  hhhNhNubh8)��}�(h�**contiguous()**�h]�h�contiguous()�����}�(hj	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj�  ubh�f on a Tensor that already has contiguous status will return itself, and no new object will be created!�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMDhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  hhhh,hNubeh}�(h!]��
contiguous�ah#]�h%]��
contiguous�ah']�h)]�uh+h
hhhhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Reshape�h]�h�Reshape�����}�(hj*	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj'	  hhhh,hMHubh.)��}�(h��Reshape is an operation that combines/splits indices of a Tensor while keeping the same total number of elements. **Tensor.reshape()** always creates a new object, but whether the internal storage is shared or not follows the rules:�h]�(h�rReshape is an operation that combines/splits indices of a Tensor while keeping the same total number of elements. �����}�(hj8	  hhhNhNubh8)��}�(h�**Tensor.reshape()**�h]�h�Tensor.reshape()�����}�(hj@	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj8	  ubh�b always creates a new object, but whether the internal storage is shared or not follows the rules:�����}�(hj8	  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMJhj'	  hhubh�)��}�(hhh]�(h�)��}�(h�jIf the Tensor object is in *contiguous* status, then only the *meta* is changed, and the storage is shared�h]�h.)��}�(hj]	  h]�(h�If the Tensor object is in �����}�(hj_	  hhhNhNubjy  )��}�(h�*contiguous*�h]�h�
contiguous�����}�(hjf	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj_	  ubh� status, then only the �����}�(hj_	  hhhNhNubjy  )��}�(h�*meta*�h]�h�meta�����}�(hjx	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj_	  ubh�& is changed, and the storage is shared�����}�(hj_	  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMMhj[	  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjX	  hhhh,hNubh�)��}�(h��If the Tensor object is in *non-contiguous* status, then the *contiguous()* will be called first before the *meta* will be changed.







�h]�h.)��}�(h��If the Tensor object is in *non-contiguous* status, then the *contiguous()* will be called first before the *meta* will be changed.�h]�(h�If the Tensor object is in �����}�(hj�	  hhhNhNubjy  )��}�(h�*non-contiguous*�h]�h�non-contiguous�����}�(hj�	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj�	  ubh� status, then the �����}�(hj�	  hhhNhNubjy  )��}�(h�*contiguous()*�h]�h�contiguous()�����}�(hj�	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj�	  ubh�! will be called first before the �����}�(hj�	  hhhNhNubjy  )��}�(h�*meta*�h]�h�meta�����}�(hj�	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jx  hj�	  ubh� will be changed.�����}�(hj�	  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hMNhj�	  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjX	  hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]�jT  jU  jV  hjW  jX  uh+h�hj'	  hhhh,hMMubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�h�guide/basic_obj/Tensor_8_cp��entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+j�	  hh,hMWhj�	  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+j�	  hj'	  hhhh,hMWubeh}�(h!]��reshape�ah#]�h%]��reshape�ah']�h)]�uh+h
hhhhhh,hMHubeh}�(h!]��when-will-data-be-copied�ah#]�h%]��when will data be copied?�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jB
  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j
  j
  j�  j�  j�  j�  j$	  j!	  j
  j
  u�	nametypes�}�(j
  �j�  �j�  �j$	  �j
  �uh!}�(j
  hj�  j_  j�  j�  j!	  j�  j
  j'	  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.