���Y      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Create a Storage�h]�h	�Text����Create a Storage�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�M/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/basic_obj/Storage_1_create.rst�hKubh	�	paragraph���)��}�(h��The storage can be created in a similar way as in Tensor. Note that Storage does not have the concept of *shape*, and behaves basically just like a **vector** in C++.�h]�(h�iThe storage can be created in a similar way as in Tensor. Note that Storage does not have the concept of �����}�(hh/hhhNhNubh	�emphasis���)��}�(h�*shape*�h]�h�shape�����}�(hh9hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh�$, and behaves basically just like a �����}�(hh/hhhNhNubh	�strong���)��}�(h�
**vector**�h]�h�vector�����}�(hhMhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhh/ubh� in C++.�����}�(hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h�7To create a Storage, with dtype=Type.Double on the cpu:�h]�h�7To create a Storage, with dtype=Type.Double on the cpu:�����}�(hhehhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hh~hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhzubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhhuhhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+hshh,hKhhhhubh	�literal_block���)��}�(h�_A = cytnx.Storage(10,dtype=cytnx.Type.Double,device=cytnx.Device.cpu)
A.set_zeros();

print(A);�h]�h�_A = cytnx.Storage(10,dtype=cytnx.Type.Double,device=cytnx.Device.cpu)
A.set_zeros();

print(A);�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��linenos���force���language��python��highlight_args�}�uh+h�hh,hK	hhhhubht)��}�(hhh]�hy)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhh�hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hKhhhhubh�)��}�(h�dauto A = cytnx::Storage(10,cytnx::Type.Double,cytnx::Device.cpu);
A.set_zeros();

cout << A << endl;�h]�h�dauto A = cytnx::Storage(10,cytnx::Type.Double,cytnx::Device.cpu);
A.set_zeros();

cout << A << endl;�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hKhhhhubh.)��}�(h�Output>>�h]�h�Output>>�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh�)��}�(h��dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]�h]�h��dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hKhhhhubh	�note���)��}�(h��[Deprecated] Storage by itself only allocates memory (using malloc) without initializing its elements.

[v0.6.6+] Storage behaves like a vector and initializes all elements to zero.�h]�(h.)��}�(h�f[Deprecated] Storage by itself only allocates memory (using malloc) without initializing its elements.�h]�h�f[Deprecated] Storage by itself only allocates memory (using malloc) without initializing its elements.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK'hj  ubh.)��}�(h�M[v0.6.6+] Storage behaves like a vector and initializes all elements to zero.�h]�h�M[v0.6.6+] Storage behaves like a vector and initializes all elements to zero.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK)hj  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j  hhhhhh,hNubh	�tip���)��}�(h��1. Use **Storage.set_zeros()** or **Storage.fill()** if you want to set all the elements to zero or some arbitrary numbers.
2. For complex type Storage, you can use **.real()** and **.imag()** to get the real part/imaginary part of the data.�h]�h	�enumerated_list���)��}�(hhh]�(hy)��}�(h�xUse **Storage.set_zeros()** or **Storage.fill()** if you want to set all the elements to zero or some arbitrary numbers.�h]�h.)��}�(hj6  h]�(h�Use �����}�(hj8  hhhNhNubhL)��}�(h�**Storage.set_zeros()**�h]�h�Storage.set_zeros()�����}�(hj?  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj8  ubh� or �����}�(hj8  hhhNhNubhL)��}�(h�**Storage.fill()**�h]�h�Storage.fill()�����}�(hjQ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj8  ubh�G if you want to set all the elements to zero or some arbitrary numbers.�����}�(hj8  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK-hj4  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj1  ubhy)��}�(h�rFor complex type Storage, you can use **.real()** and **.imag()** to get the real part/imaginary part of the data.�h]�h.)��}�(hjq  h]�(h�&For complex type Storage, you can use �����}�(hjs  hhhNhNubhL)��}�(h�**.real()**�h]�h�.real()�����}�(hjz  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhjs  ubh� and �����}�(hjs  hhhNhNubhL)��}�(h�**.imag()**�h]�h�.imag()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhjs  ubh�1 to get the real part/imaginary part of the data.�����}�(hjs  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK.hjo  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj1  ubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+j/  hj+  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j)  hhhhhNhNubh)��}�(hhh]�(h)��}�(h�Type conversion�h]�h�Type conversion�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK3ubh.)��}�(h��Conversion between different data types is possible for Storage. Just like Tensor, call **Storage.astype()** to convert between different data types.�h]�(h�XConversion between different data types is possible for Storage. Just like Tensor, call �����}�(hj�  hhhNhNubhL)��}�(h�**Storage.astype()**�h]�h�Storage.astype()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj�  ubh�) to convert between different data types.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK4hj�  hhubh.)��}�(h�4The available data types are the same as for Tensor.�h]�h�4The available data types are the same as for Tensor.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK6hj�  hhubht)��}�(hhh]�hy)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK8hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hK8hj�  hhubh�)��}�(h�^A = cytnx.Storage(10)
A.set_zeros()

B = A.astype(cytnx.Type.ComplexDouble)

print(A)
print(B)�h]�h�^A = cytnx.Storage(10)
A.set_zeros()

B = A.astype(cytnx.Type.ComplexDouble)

print(A)
print(B)�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK:hj�  hhubht)��}�(hhh]�hy)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj2  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKEhj.  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj+  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hKEhj�  hhubh�)��}�(h��auto A = cytnx::Storage(10);
A.set_zeros();

auto B = A.astype(cytnx::Type.ComplexDouble);

cout << A << endl;
cout << B << endl;�h]�h��auto A = cytnx::Storage(10);
A.set_zeros();

auto B = A.astype(cytnx::Type.ComplexDouble);

cout << A << endl;
cout << B << endl;�����}�hjL  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hKGhj�  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj\  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKRhj�  hhubh�)��}�(hX  dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]


dtype : Complex Double (Complex Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j  ]�h]�hX  dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 0.00000e+00 ]


dtype : Complex Double (Complex Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j 0.00000e+00+0.00000e+00j  ]�����}�hjj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hKThj�  hhubeh}�(h!]��type-conversion�ah#]�h%]��type conversion�ah']�h)]�uh+h
hhhhhh,hK3ubh)��}�(hhh]�(h)��}�(h�Transfer between devices�h]�h�Transfer between devices�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKcubh.)��}�(h�kWe can also transfer the storage between different devices. Similar to Tensor, we can use **Storage.to()**.�h]�(h�ZWe can also transfer the storage between different devices. Similar to Tensor, we can use �����}�(hj�  hhhNhNubhL)��}�(h�**Storage.to()**�h]�h�Storage.to()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj�  ubh�.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKdhj�  hhubht)��}�(hhh]�hy)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKfhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hKfhj�  hhubh�)��}�(h��A = cytnx.Storage(4)
B = A.to(cytnx.Device.cuda)

print(A.device_str())
print(B.device_str())

A.to_(cytnx.Device.cuda)
print(A.device_str())�h]�h��A = cytnx.Storage(4)
B = A.to(cytnx.Device.cuda)

print(A.device_str())
print(B.device_str())

A.to_(cytnx.Device.cuda)
print(A.device_str())�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hKhhj�  hhubht)��}�(hhh]�hy)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKuhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hKuhj�  hhubh�)��}�(h��auto A = cytnx::Storage(4);

auto B = A.to(cytnx::Device.cuda);
cout << A.device_str() << endl;
cout << B.device_str() << endl;

A.to_(cytnx::Device.cuda);
cout << A.device_str() << endl;�h]�h��auto A = cytnx::Storage(4);

auto B = A.to(cytnx::Device.cuda);
cout << A.device_str() << endl;
cout << B.device_str() << endl;

A.to_(cytnx::Device.cuda);
cout << A.device_str() << endl;�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hKwhj�  hhubh.)��}�(h�Output>>�h]�h�Output>>�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(h�Icytnx device: CPU
cytnx device: CUDA/GPU-id:0
cytnx device: CUDA/GPU-id:0�h]�h�Icytnx device: CPU
cytnx device: CUDA/GPU-id:0
cytnx device: CUDA/GPU-id:0�����}�hj#  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hK�hj�  hhubh	�hint���)��}�(h��1. Like Tensor, **.device_str()** returns the device string while **.device()** returns device ID (cpu=-1).

2. **.to()** returns a copy on the target device. Use **.to_()** instead to move the current instance to a target device.�h]�j0  )��}�(hhh]�(hy)��}�(h�iLike Tensor, **.device_str()** returns the device string while **.device()** returns device ID (cpu=-1).
�h]�h.)��}�(h�hLike Tensor, **.device_str()** returns the device string while **.device()** returns device ID (cpu=-1).�h]�(h�Like Tensor, �����}�(hj@  hhhNhNubhL)��}�(h�**.device_str()**�h]�h�.device_str()�����}�(hjH  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj@  ubh�! returns the device string while �����}�(hj@  hhhNhNubhL)��}�(h�**.device()**�h]�h�	.device()�����}�(hjZ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj@  ubh� returns device ID (cpu=-1).�����}�(hj@  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj<  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj9  ubhy)��}�(h�v**.to()** returns a copy on the target device. Use **.to_()** instead to move the current instance to a target device.�h]�h.)��}�(hjz  h]�(hL)��}�(h�	**.to()**�h]�h�.to()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj|  ubh�* returns a copy on the target device. Use �����}�(hj|  hhhNhNubhL)��}�(h�
**.to_()**�h]�h�.to_()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj|  ubh�9 instead to move the current instance to a target device.�����}�(hj|  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjx  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj9  ubeh}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  hj�  j�  uh+j/  hj5  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j3  hj�  hhhNhNubeh}�(h!]��transfer-between-devices�ah#]�h%]��transfer between devices�ah']�h)]�uh+h
hhhhhh,hKcubh)��}�(hhh]�(h)��}�(h�Get Storage of Tensor�h]�h�Get Storage of Tensor�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h�uInternally, the data of a Tensor is stored in Storage. We can get the storage of a Tensor using **Tensor.storage()**.�h]�(h�`Internally, the data of a Tensor is stored in Storage. We can get the storage of a Tensor using �����}�(hj�  hhhNhNubhL)��}�(h�**Tensor.storage()**�h]�h�Tensor.storage()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj�  ubh�.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubht)��}�(hhh]�hy)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hK�hj�  hhubh�)��}�(h�FA = cytnx.arange(10).reshape(2,5);
B = A.storage();

print(A)
print(B)�h]�h�FA = cytnx.arange(10).reshape(2,5);
B = A.storage();

print(A)
print(B)�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK�hj�  hhubht)��}�(hhh]�hy)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj,  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj(  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj%  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hK�hj�  hhubh�)��}�(h�eauto A = cytnx::arange(10).reshape(2,5);
auto B = A.storage();

cout << A << endl;
cout << B << endl;�h]�h�eauto A = cytnx::arange(10).reshape(2,5);
auto B = A.storage();

cout << A << endl;
cout << B << endl;�����}�hjF  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hK�hj�  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(hX�  Total elem: 10
type  : Double (Float64)
cytnx device: CPU
Shape : (2,5)
[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 ]
 [5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 ]]


dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 ]�h]�hX�  Total elem: 10
type  : Double (Float64)
cytnx device: CPU
Shape : (2,5)
[[0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 ]
 [5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 ]]


dtype : Double (Float64)
device: cytnx device: CPU
size  : 10
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 8.00000e+00 9.00000e+00 ]�����}�hjd  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hK�hj�  hhubj  )��}�(h��1. The return value is a *reference* to the Tensor's internal storage. This implies that any modification to this storage will modify the Tensor accordingly.�h]�j0  )��}�(hhh]�hy)��}�(h��The return value is a *reference* to the Tensor's internal storage. This implies that any modification to this storage will modify the Tensor accordingly.�h]�h.)��}�(hj}  h]�(h�The return value is a �����}�(hj  hhhNhNubh8)��}�(h�*reference*�h]�h�	reference�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hj  ubh�{ to the Tensor’s internal storage. This implies that any modification to this storage will modify the Tensor accordingly.�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj{  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhjx  ubah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  hj�  j�  uh+j/  hjt  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j  hj�  hhhNhNubh.)��}�(hX	  **[Important]** For a Tensor in non-contiguous status, the meta-data is detached from its memory handled by storage. In this case, calling **Tensor.storage()** will return the current memory layout, not the ordering according to the Tensor indices in the meta-data.�h]�(hL)��}�(h�**[Important]**�h]�h�[Important]�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj�  ubh�| For a Tensor in non-contiguous status, the meta-data is detached from its memory handled by storage. In this case, calling �����}�(hj�  hhhNhNubhL)��}�(h�**Tensor.storage()**�h]�h�Tensor.storage()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hKhj�  ubh�j will return the current memory layout, not the ordering according to the Tensor indices in the meta-data.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh.)��}�(h�SLet's use Python API to demonstrate this. The c++ API can be used in a similar way.�h]�h�ULet’s use Python API to demonstrate this. The c++ API can be used in a similar way.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubht)��}�(hhh]�hy)��}�(h�In Python:
�h]�h.)��}�(h�
In Python:�h]�h�
In Python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hxhj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�h�uh+hshh,hK�hj�  hhubh�)��}�(hX]  A = cytnx.arange(8).reshape(2,2,2)
print(A.storage())

# Let's make it non-contiguous
A.permute_(0,2,1)
print(A.is_contiguous())

# Note that the storage is not changed
print(A.storage())

# Now let's make it contiguous
# thus the elements is moved
A.contiguous_();
print(A.is_contiguous())

# Note that the storage now is changed
print(A.storage())�h]�hX]  A = cytnx.arange(8).reshape(2,2,2)
print(A.storage())

# Let's make it non-contiguous
A.permute_(0,2,1)
print(A.is_contiguous())

# Note that the storage is not changed
print(A.storage())

# Now let's make it contiguous
# thus the elements is moved
A.contiguous_();
print(A.is_contiguous())

# Note that the storage now is changed
print(A.storage())�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK�hj�  hhubh.)��}�(h�Output>>�h]�h�Output>>�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(hX�  dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]

False

dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]

True

dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 2.00000e+00 1.00000e+00 3.00000e+00 4.00000e+00 6.00000e+00 5.00000e+00 7.00000e+00 ]�h]�hX�  dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]

False

dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 1.00000e+00 2.00000e+00 3.00000e+00 4.00000e+00 5.00000e+00 6.00000e+00 7.00000e+00 ]

True

dtype : Double (Float64)
device: cytnx device: CPU
size  : 8
[ 0.00000e+00 2.00000e+00 1.00000e+00 3.00000e+00 4.00000e+00 6.00000e+00 5.00000e+00 7.00000e+00 ]�����}�hj+  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hK�hj�  hhubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�h� guide/basic_obj/Storage_1_create��entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+j@  hh,hK�hj=  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+j;  hj�  hhhh,hK�ubeh}�(h!]��get-storage-of-tensor�ah#]�h%]��get storage of tensor�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]��create-a-storage�ah#]�h%]��create a storage�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jm  jj  j  j|  j�  j�  je  jb  u�	nametypes�}�(jm  �j  �j�  �je  �uh!}�(jj  hj|  j�  j�  j�  jb  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.