��*S      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Network�h]�h	�Text����Network�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�>/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/cyx/Network.rst�hKubh	�	paragraph���)��}�(hX�  Network is a class for contracting UniTensors, it is useful when we have to perform the same large contraction task many times.
We can create configuration for the contraction task and put the "constant" UniTensors in at the initialization step of some algorithms,
later when runing the sweeping or the iterative steps, we put the variational tensors in and launch the network to get the results.�h]�hX�  Network is a class for contracting UniTensors, it is useful when we have to perform the same large contraction task many times.
We can create configuration for the contraction task and put the “constant” UniTensors in at the initialization step of some algorithms,
later when runing the sweeping or the iterative steps, we put the variational tensors in and launch the network to get the results.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Network from .net file�h]�h�Network from .net file�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hK	ubh.)��}�(h�dLet's take the corner transfer matrix for example, first we draw the desired tensor network diagram:�h]�h�fLet’s take the corner transfer matrix for example, first we draw the desired tensor network diagram:�����}�(hhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�image���)��}�(h�<.. image:: image/ctm.png
    :width: 300
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��300��align��center��uri��guide/cyx/image/ctm.png��
candidates�}��*�hmsuh+h\hh=hhhh,hNubh.)��}�(h�gWe now convert the diagram to the .net file to represent the contraction task, which is straghtforward:�h]�h�gWe now convert the diagram to the .net file to represent the contraction task, which is straghtforward:�����}�(hhqhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(h�	ctm.net:
�h]�h.)��}�(h�ctm.net:�h]�h�ctm.net:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet�hpuh+hhh,hKhh=hhubh	�literal_block���)��}�(h��c1: 0,2
t1: 1,3,0
c2: 4,1
t2: 9,6,4
c3: 11, 9
t3: 10, 8, 11
c4: 7, 10
t4: 2,5,7
w: 3,6,8,5
TOUT:
ORDER: ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)�h]�h��c1: 0,2
t1: 1,3,0
c2: 4,1
t2: 9,6,4
c3: 11, 9
t3: 10, 8, 11
c4: 7, 10
t4: 2,5,7
w: 3,6,8,5
TOUT:
ORDER: ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��linenos���force���language��text��highlight_args�}�uh+h�hh,hKhh=hhubh.)��}�(h�
Note that:�h]�h�
Note that:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK$hh=hhubh	�enumerated_list���)��}�(hhh]�(h�)��}�(h�iThe labels above correspond to the diagram you draw, not the label attribute of UniTensor object itself.
�h]�h.)��}�(h�hThe labels above correspond to the diagram you draw, not the label attribute of UniTensor object itself.�h]�h�hThe labels above correspond to the diagram you draw, not the label attribute of UniTensor object itself.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK&hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h��Labels should be seperated by ' , ', and ' ; ' seperate the labels in rowspace and colspace. In the above case all legs live in the colspace.
�h]�h.)��}�(h��Labels should be seperated by ' , ', and ' ; ' seperate the labels in rowspace and colspace. In the above case all legs live in the colspace.�h]�h��Labels should be seperated by ‘ , ‘, and ‘ ; ‘ seperate the labels in rowspace and colspace. In the above case all legs live in the colspace.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h�jTOUT specify the output configuration, in this case we leave it blank since we will get a scalar outcome.
�h]�h.)��}�(h�iTOUT specify the output configuration, in this case we leave it blank since we will get a scalar outcome.�h]�h�iTOUT specify the output configuration, in this case we leave it blank since we will get a scalar outcome.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK*hj   ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h�FORDER is optional and used to specify the contraction order manually.
�h]�h.)��}�(h�EORDER is optional and used to specify the contraction order manually.�h]�h�EORDER is optional and used to specify the contraction order manually.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK,hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+h�hh=hhhh,hK&ubeh}�(h!]��network-from-net-file�ah#]�h%]��network from .net file�ah']�h)]�uh+h
hhhhhh,hK	ubh)��}�(hhh]�(h)��}�(h�Put UniTensors and Launch�h]�h�Put UniTensors and Launch�����}�(hjF  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjC  hhhh,hK/ubh.)��}�(h�mTo use, we simply create the Network object (at the same time we load the .net file), and put the UniTensors:�h]�h�mTo use, we simply create the Network object (at the same time we load the .net file), and put the UniTensors:�����}�(hjT  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK0hjC  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hji  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hje  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjb  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK2hjC  hhubh�)��}�(h�vN = Network("ctm.net")
N.PutUniTensor("c1",c1)
N.PutUniTensor("c2",c2)
print(N)
N.PutUniTensor("c3",c3)
# and so on...�h]�h�vN = Network("ctm.net")
N.PutUniTensor("c1",c1)
N.PutUniTensor("c2",c2)
print(N)
N.PutUniTensor("c3",c3)
# and so on...�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK4hjC  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK>hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK>hjC  hhubh�)��}�(h��Network N = Network("ctm.net");
N.PutUniTensor("c1",c1);
N.PutUniTensor("c2",c2);
cout << N;
N.PutUniTensor("c3",c3)
// and so on...�h]�h��Network N = Network("ctm.net");
N.PutUniTensor("c1",c1);
N.PutUniTensor("c2",c2);
cout << N;
N.PutUniTensor("c3",c3)
// and so on...�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hK@hjC  hhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKJhjC  hhubh�)��}�(h��==== Network ====
[o] c1 : 0 2
[x] t1 : 1 3 0
[o] c2 : 4 1
[x] t2 : 9 6 4
[x] c3 : 11 9
[x] t3 : 10 8 11
[x] c4 : 7 10
[x] t4 : 2 5 7
[x] w : 3 6 8 5
TOUT : ;
ORDER : ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)
=================�h]�h��==== Network ====
[o] c1 : 0 2
[x] t1 : 1 3 0
[o] c2 : 4 1
[x] t2 : 9 6 4
[x] c3 : 11 9
[x] t3 : 10 8 11
[x] c4 : 7 10
[x] t4 : 2 5 7
[x] w : 3 6 8 5
TOUT : ;
ORDER : ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)
=================�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��text�h�}�uh+h�hh,hKLhjC  hhubh.)��}�(h�DTo perform the contraction and get the outcome, we use the Launch():�h]�h�DTo perform the contraction and get the outcome, we use the Launch():�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK\hjC  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK^hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK^hjC  hhubh�)��}�(h�Res = N.Launch(optimal = True)�h]�h�Res = N.Launch(optimal = True)�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK`hjC  hhubh�)��}�(hhh]�h�)��}�(h�In C++:
�h]�h.)��}�(h�In C++:�h]�h�In C++:�����}�(hj(  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKehj$  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj!  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hKehjC  hhubh�)��}�(h�UniTensor Res = N.Launch(true)�h]�h�UniTensor Res = N.Launch(true)�����}�hjB  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��c++�h�}�uh+h�hh,hKghjC  hhubh.)��}�(h��Here if the argument **optimal = True**, the contraction order is always auto-optimized.
If **optimal = False**, the specified ORDER in network file will be used, if there is even no specified order, tensors are contracted one by one in sequence.�h]�(h�Here if the argument �����}�(hjR  hhhNhNubh	�strong���)��}�(h�**optimal = True**�h]�h�optimal = True�����}�(hj\  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hjR  ubh�5, the contraction order is always auto-optimized.
If �����}�(hjR  hhhNhNubj[  )��}�(h�**optimal = False**�h]�h�optimal = False�����}�(hjn  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hjR  ubh��, the specified ORDER in network file will be used, if there is even no specified order, tensors are contracted one by one in sequence.�����}�(hjR  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKlhjC  hhubh	�note���)��}�(hXk  The auto-optimized contraction order obtained by calling **.Launch(optimal = True)** will save in the Network object, so if there is no need to re-optimize the order (i.e. Bond dimensions of the input tensors remain the same.), next time when we call **.Launch()** again, we should set **optimal = False** to avoid the overhead of recalculating the optimal order.�h]�h.)��}�(hj�  h]�(h�9The auto-optimized contraction order obtained by calling �����}�(hj�  hhhNhNubj[  )��}�(h�**.Launch(optimal = True)**�h]�h�.Launch(optimal = True)�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hj�  ubh�� will save in the Network object, so if there is no need to re-optimize the order (i.e. Bond dimensions of the input tensors remain the same.), next time when we call �����}�(hj�  hhhNhNubj[  )��}�(h�**.Launch()**�h]�h�	.Launch()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hj�  ubh� again, we should set �����}�(hj�  hhhNhNubj[  )��}�(h�**optimal = False**�h]�h�optimal = False�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hj�  ubh�: to avoid the overhead of recalculating the optimal order.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKqhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hjC  hhhh,hNubeh}�(h!]��put-unitensors-and-launch�ah#]�h%]��put unitensors and launch�ah']�h)]�uh+h
hhhhhh,hK/ubh)��}�(hhh]�(h)��}�(h�Network from string�h]�h�Network from string�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKtubh.)��}�(h�ZAlternatively, we can implement the contraction directly in the program with FromString():�h]�h�ZAlternatively, we can implement the contraction directly in the program with FromString():�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKuhj�  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKwhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hKwhj�  hhubh�)��}�(hXb  N = cytnx.Network()
N.FromString(["c1: 0, 2",\
            "t1: 1, 3, 0",\
            "c2: 4, 1",\
            "t2: 9, 6, 4",\
            "c3: 11, 9",\
            "t3: 10, 8, 11",\
            "c4: 7, 10",\
            "t4: 2, 5, 7",\
            "w: 3, 6, 8, 5",\
            "TOUT:",\
            "ORDER: ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)"])�h]�hXb  N = cytnx.Network()
N.FromString(["c1: 0, 2",\
            "t1: 1, 3, 0",\
            "c2: 4, 1",\
            "t2: 9, 6, 4",\
            "c3: 11, 9",\
            "t3: 10, 8, 11",\
            "c4: 7, 10",\
            "t4: 2, 5, 7",\
            "w: 3, 6, 8, 5",\
            "TOUT:",\
            "ORDER: ((((((((c1,t1),c2),t4),w),t2),c4),t3),c3)"])�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hKyhj�  hhubh.)��}�(h�eThis approach should be convenient when you don't want to maintain the .net file outside the program.�h]�h�gThis approach should be convenient when you don’t want to maintain the .net file outside the program.�����}�(hj-  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubeh}�(h!]��network-from-string�ah#]�h%]��network from string�ah']�h)]�uh+h
hhhhhh,hKtubh)��}�(hhh]�(h)��}�(h�(PutUniTensor according to label ordering�h]�h�(PutUniTensor according to label ordering�����}�(hjF  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjC  hhhh,hK�ubh.)��}�(hX<  When we put a UniTensor into a Network, we can also specify its leg order according to a label ordering, this interface turns out to be convinient
since users don't need to memorize or look up the index of s desired leg. To be more specific, consider
a example, we grab two three leg tensors **A1** and **A2**, they both have one leg that spans the physical space and the other two legs describe
the virtual space (such tensors are often appearing as the building block tensors of matrix product state), we create the tensors and set the corresponding lebels
as following:�h]�(hX&  When we put a UniTensor into a Network, we can also specify its leg order according to a label ordering, this interface turns out to be convinient
since users don’t need to memorize or look up the index of s desired leg. To be more specific, consider
a example, we grab two three leg tensors �����}�(hjT  hhhNhNubj[  )��}�(h�**A1**�h]�h�A1�����}�(hj\  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hjT  ubh� and �����}�(hjT  hhhNhNubj[  )��}�(h�**A2**�h]�h�A2�����}�(hjn  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hjT  ubhX  , they both have one leg that spans the physical space and the other two legs describe
the virtual space (such tensors are often appearing as the building block tensors of matrix product state), we create the tensors and set the corresponding lebels
as following:�����}�(hjT  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjC  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK�hjC  hhubh�)��}�(h��A1 = cytnx.UniTensor(cytnx.ones([2,8,8]));
A1.set_labels(["phy","v1","v2"])
A2 = cytnx.UniTensor(cytnx.ones([2,8,8]));
A2.set_labels(["phy","v1","v2"])�h]�h��A1 = cytnx.UniTensor(cytnx.ones([2,8,8]));
A1.set_labels(["phy","v1","v2"])
A2 = cytnx.UniTensor(cytnx.ones([2,8,8]));
A2.set_labels(["phy","v1","v2"])�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK�hjC  hhubh.)��}�(h��The legs of these tensors are arranged such that the first leg is the physical leg (with dimension 2 for spin-half case for example) and the other two legs are
the virtual ones (with dimension 8).�h]�h��The legs of these tensors are arranged such that the first leg is the physical leg (with dimension 2 for spin-half case for example) and the other two legs are
the virtual ones (with dimension 8).�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjC  hhubh.)��}�(h�pNow suppose somehow we want to contract these two tensors by its physical legs, we create the following Network:�h]�h�pNow suppose somehow we want to contract these two tensors by its physical legs, we create the following Network:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjC  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK�hjC  hhubh�)��}�(h�iN = cytnx.Network()
N.FromString(["A1: 1,-1,2",\
            "A2: 3,-1,4",\
            "TOUT: 1,3;2,4"])�h]�h�iN = cytnx.Network()
N.FromString(["A1: 1,-1,2",\
            "A2: 3,-1,4",\
            "TOUT: 1,3;2,4"])�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK�hjC  hhubh.)��}�(h��Note that in this Network it is the second leg of the two tensors to be contracted, which will not be consistent since **A1** and **A2**
are created such that their physical leg is the first one, while we can do the following:�h]�(h�wNote that in this Network it is the second leg of the two tensors to be contracted, which will not be consistent since �����}�(hj  hhhNhNubj[  )��}�(h�**A1**�h]�h�A1�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hj  ubh� and �����}�(hj  hhhNhNubj[  )��}�(h�**A2**�h]�h�A2�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jZ  hj  ubh�Z
are created such that their physical leg is the first one, while we can do the following:�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjC  hhubh�)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj=  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj9  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj6  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]�h�hpuh+hhh,hK�hjC  hhubh�)��}�(h�SN.PutUniTensor("A1",A1,["v1","phy","v2"])
N.PutUniTensor("A2",A2,["v1","phy","v2"])�h]�h�SN.PutUniTensor("A1",A1,["v1","phy","v2"])
N.PutUniTensor("A2",A2,["v1","phy","v2"])�����}�hjW  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��h��python�h�}�uh+h�hh,hK�hjC  hhubh.)��}�(h��So when we do the PutUniTensor() we add the third arguement which is a labels ordering, what this function will do is nothing but permute
the tensor legs according to this label ordering before putting them into the Network.�h]�h��So when we do the PutUniTensor() we add the third arguement which is a labels ordering, what this function will do is nothing but permute
the tensor legs according to this label ordering before putting them into the Network.�����}�(hjg  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjC  hhubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�h�guide/cyx/Network��entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+jz  hh,hK�hjw  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+ju  hjC  hhhh,hK�ubeh}�(h!]��(putunitensor-according-to-label-ordering�ah#]�h%]��(putunitensor according to label ordering�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]��network�ah#]�h%]��network�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j@  j=  j�  j�  j@  j=  j�  j�  u�	nametypes�}�(j�  �j@  �j�  �j@  �j�  �uh!}�(j�  hj=  h=j�  jC  j=  j�  j�  jC  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.