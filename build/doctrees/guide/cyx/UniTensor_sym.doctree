��}=      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�UniTensor with Symmetry�h]�h	�Text����UniTensor with Symmetry�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�D/home/kaihsinwu/Dropbox/Cytnx_doc/source/guide/cyx/UniTensor_sym.rst�hKubh	�	paragraph���)��}�(hX�  Tensor with symmetries has several advantages in physical simulations. In a system with symmetry, Hamiltonian can be block-diagonalized into symmetry sectors of charge (quantum numbers). When Impose symmetry structure into our tensor, by ultilizing this block-diagonalize structure, we can substantially reduce the number of variational parameters, allowing us to go to larger system sizes/effective virtual bond dimension.�h]�hX�  Tensor with symmetries has several advantages in physical simulations. In a system with symmetry, Hamiltonian can be block-diagonalized into symmetry sectors of charge (quantum numbers). When Impose symmetry structure into our tensor, by ultilizing this block-diagonalize structure, we can substantially reduce the number of variational parameters, allowing us to go to larger system sizes/effective virtual bond dimension.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h��The quanutm number conserving Tensor can be understanding in a simple way. Each bond (leg) of the Tensor carries quantum numbers and is directional, either pointing in or pointing out. As shown in the following figure:�h]�h��The quanutm number conserving Tensor can be understanding in a simple way. Each bond (leg) of the Tensor carries quantum numbers and is directional, either pointing in or pointing out. As shown in the following figure:�����}�(hh=hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�image���)��}�(h�>.. image:: image/ut_bd.png
    :width: 600
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��600��align��center��uri��guide/cyx/image/ut_bd.png��
candidates�}��*�h\suh+hKhhhhhh,hNubh.)��}�(hXr  The conservation of quantum number (symmetry charge) indicates that only elements with **zero flux** will be allow to exist (total quantum number flows into the tensor subject to the combine rule of symmetry is equal to total quantum number flow out), therefore belongs to a valid block. Other elements that belongs to non-zero flux will not be created in our UniTensor.�h]�(h�WThe conservation of quantum number (symmetry charge) indicates that only elements with �����}�(hh`hhhNhNubh	�strong���)��}�(h�**zero flux**�h]�h�	zero flux�����}�(hhjhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhh`ubhX   will be allow to exist (total quantum number flows into the tensor subject to the combine rule of symmetry is equal to total quantum number flow out), therefore belongs to a valid block. Other elements that belongs to non-zero flux will not be created in our UniTensor.�����}�(hh`hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubhL)��}�(h�C.. image:: image/ut_blocks.png
    :width: 600
    :align: center

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��600��align��center��uri��guide/cyx/image/ut_blocks.png�h]}�h_h�suh+hKhhhhhh,hNubh.)��}�(h�@To impose the symmetry, there are only two things we need to do:�h]�h�@To impose the symmetry, there are only two things we need to do:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(h�BIdentify the symmetries in the system (for example, U(1) symmetry)�h]�h.)��}�(hh�h]�h�BIdentify the symmetries in the system (for example, U(1) symmetry)�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h�\Creat **directional** Bonds that carries quantum numbers flow associate to that symmetries.
�h]�h.)��}�(h�[Creat **directional** Bonds that carries quantum numbers flow associate to that symmetries.�h]�(h�Creat �����}�(hh�hhhNhNubhi)��}�(h�**directional**�h]�h�directional�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhh�ubh�F Bonds that carries quantum numbers flow associate to that symmetries.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+h�hhhhhh,hKubh.)��}�(h�\As an simple example, lets creat a 3-rank tensor with U(1) symmetry as the following figure:�h]�h�\As an simple example, lets creat a 3-rank tensor with U(1) symmetry as the following figure:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubhL)��}�(h�@.. image:: image/u1_tdex.png
    :width: 500
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��500��align��center��uri��guide/cyx/image/u1_tdex.png�h]}�h_j  suh+hKhhhhhh,hNubh.)��}�(h��Here, we use the notation *{Qnum}>>dimension*. First, three bonds  **bond_c** (in), **bond_d** (in) and **bond_e** (out) are created with corresponding qnums (see 7.2 for further info related to Bond). Then we initialize our UniTensor **Td** using them:�h]�(h�Here, we use the notation �����}�(hj  hhhNhNubh	�emphasis���)��}�(h�*{Qnum}>>dimension*�h]�h�{Qnum}>>dimension�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j  hj  ubh�. First, three bonds  �����}�(hj  hhhNhNubhi)��}�(h�
**bond_c**�h]�h�bond_c�����}�(hj/  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj  ubh� (in), �����}�(hj  hhhNhNubhi)��}�(h�
**bond_d**�h]�h�bond_d�����}�(hjA  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj  ubh�
 (in) and �����}�(hj  hhhNhNubhi)��}�(h�
**bond_e**�h]�h�bond_e�����}�(hjS  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj  ubh�y (out) are created with corresponding qnums (see 7.2 for further info related to Bond). Then we initialize our UniTensor �����}�(hj  hhhNhNubhi)��}�(h�**Td**�h]�h�Td�����}�(hje  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj  ubh� using them:�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�bullet_list���)��}�(hhh]�h�)��}�(h�In python:
�h]�h.)��}�(h�
In python:�h]�h�
In python:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet�h_uh+j}  hh,hK!hhhhubh	�literal_block���)��}�(hXH  bond_c = cytnx.Bond(cytnx.BD_IN, [Qs(1)>>1, Qs(-1)>>1],[cytnx.Symmetry.U1()])
bond_d = cytnx.Bond(cytnx.BD_IN, [Qs(1)>>1, Qs(-1)>>1],[cytnx.Symmetry.U1()])
bond_e = cytnx.Bond(cytnx.BD_OUT, [Qs(2)>>1, Qs(0)>>2, Qs(-2)>>1],[cytnx.Symmetry.U1()])
Td = cytnx.UniTensor([bond_c, bond_d, bond_e])
Td.set_name("Td")
Td.print_diagram()�h]�hXH  bond_c = cytnx.Bond(cytnx.BD_IN, [Qs(1)>>1, Qs(-1)>>1],[cytnx.Symmetry.U1()])
bond_d = cytnx.Bond(cytnx.BD_IN, [Qs(1)>>1, Qs(-1)>>1],[cytnx.Symmetry.U1()])
bond_e = cytnx.Bond(cytnx.BD_OUT, [Qs(2)>>1, Qs(0)>>2, Qs(-2)>>1],[cytnx.Symmetry.U1()])
Td = cytnx.UniTensor([bond_c, bond_d, bond_e])
Td.set_name("Td")
Td.print_diagram()�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��linenos���force���language��python��highlight_args�}�uh+j�  hh,hK#hhhhubh.)��}�(h�	Output >>�h]�h�	Output >>�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK.hhhhubj�  )��}�(hX?  -----------------------
tensor Name : Td
tensor Rank : 3
contiguous  : True
valid blocks : 4
is diag   : False
on device   : cytnx device: CPU
      row           col
         -----------
         |         |
   0  -->| 2     4 |-->  2
         |         |
   1  -->| 2       |
         |         |
         -----------�h]�hX?  -----------------------
tensor Name : Td
tensor Rank : 3
contiguous  : True
valid blocks : 4
is diag   : False
on device   : cytnx device: CPU
      row           col
         -----------
         |         |
   0  -->| 2     4 |-->  2
         |         |
   1  -->| 2       |
         |         |
         -----------�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hK0hhhhubh.)��}�(h��We note that in this UniTensor, there are only **4** valid blocks** that carries zero-flux, as also shown in the figure. We can use **Td.print_blocks()** to see how many blocks, and their structure:�h]�(h�/We note that in this UniTensor, there are only �����}�(hj�  hhhNhNubhi)��}�(h�**4**�h]�h�4�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj�  ubh�P valid blocks** that carries zero-flux, as also shown in the figure. We can use �����}�(hj�  hhhNhNubhi)��}�(h�**Td.print_blocks()**�h]�h�Td.print_blocks()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj�  ubh�- to see how many blocks, and their structure:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKBhhhhubj�  )��}�(hX�  -------- start of print ---------
Tensor name: Td
braket_form : True
is_diag    : False
[OVERALL] contiguous : True
========================
BLOCK [#0]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                 -----------
                 |         |
   [0] U1(1)  -->| 1     1 |-->  [0] U1(2)
                 |         |
   [0] U1(1)  -->| 1       |
                 |         |
                 -----------

Total elem: 1
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,1)
[[[0.00000e+00 ]]]

========================
BLOCK [#1]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [0] U1(1)   -->| 1     2 |-->  [1] U1(0)
                  |         |
   [1] U1(-1)  -->| 1       |
                  |         |
                  -----------

Total elem: 2
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,2)
[[[0.00000e+00 0.00000e+00 ]]]

========================
BLOCK [#2]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [1] U1(-1)  -->| 1     2 |-->  [1] U1(0)
                  |         |
   [0] U1(1)   -->| 1       |
                  |         |
                  -----------

Total elem: 2
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,2)
[[[0.00000e+00 0.00000e+00 ]]]

========================
BLOCK [#3]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [1] U1(-1)  -->| 1     1 |-->  [2] U1(-2)
                  |         |
   [1] U1(-1)  -->| 1       |
                  |         |
                  -----------

Total elem: 1
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,1)
[[[0.00000e+00 ]]]�h]�hX�  -------- start of print ---------
Tensor name: Td
braket_form : True
is_diag    : False
[OVERALL] contiguous : True
========================
BLOCK [#0]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                 -----------
                 |         |
   [0] U1(1)  -->| 1     1 |-->  [0] U1(2)
                 |         |
   [0] U1(1)  -->| 1       |
                 |         |
                 -----------

Total elem: 1
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,1)
[[[0.00000e+00 ]]]

========================
BLOCK [#1]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [0] U1(1)   -->| 1     2 |-->  [1] U1(0)
                  |         |
   [1] U1(-1)  -->| 1       |
                  |         |
                  -----------

Total elem: 2
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,2)
[[[0.00000e+00 0.00000e+00 ]]]

========================
BLOCK [#2]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [1] U1(-1)  -->| 1     2 |-->  [1] U1(0)
                  |         |
   [0] U1(1)   -->| 1       |
                  |         |
                  -----------

Total elem: 2
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,2)
[[[0.00000e+00 0.00000e+00 ]]]

========================
BLOCK [#3]
 |- []   : Qn index
 |- Sym(): Qnum of correspond symmetry
                  -----------
                  |         |
   [1] U1(-1)  -->| 1     1 |-->  [2] U1(-2)
                  |         |
   [1] U1(-1)  -->| 1       |
                  |         |
                  -----------

Total elem: 1
type  : Double (Float64)
cytnx device: CPU
Shape : (1,1,1)
[[[0.00000e+00 ]]]�����}�hj	  sbah}�(h!]�h#]�h%]�h']�h)]�j�  j�  j�  �j�  �text�j�  }�uh+j�  hh,hKDhhhhubh	�note���)��}�(h��Here, the number in the square braket **[]** in print_blocks() indicates the Qn index. A Qn index is the index where the corresponding quantum number (Qnum) resides on given bond.�h]�h.)��}�(hj  h]�(h�&Here, the number in the square braket �����}�(hj  hhhNhNubhi)��}�(h�**[]**�h]�h�[]�����}�(hj&  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhj  ubh�� in print_blocks() indicates the Qn index. A Qn index is the index where the corresponding quantum number (Qnum) resides on given bond.�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j  hhhhhh,hNubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�h�guide/cyx/UniTensor_sym��entries�]��includefiles�]��maxdepth�J�����caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh+jI  hh,hK�hjF  ubah}�(h!]�h#]��toctree-wrapper�ah%]�h']�h)]�uh+jD  hhhhhh,hK�ubeh}�(h!]��unitensor-with-symmetry�ah#]�h%]��unitensor with symmetry�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�jn  jk  s�	nametypes�}�jn  �sh!}�jk  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.